using UnityEngine;

namespace SDA.Enemy
{
    public class EnemyInstance : MonoBehaviour
    {
        [SerializeField]
        private Transform enemy;

        [SerializeField]
        private Transform startWaypoint;

        [SerializeField]
        private Transform endWaypoint;

        [SerializeField]
        private SpriteRenderer enemyRenderer;

        private bool isMovingFromStartToEnd = true;

        private float startTime;
        private float currentTime;
        
        [SerializeField]
        private float duration = 5;

        public void Init()
        {
            startTime = Time.time;
            currentTime = 0;
        }

        public void UpdatePosition()
        {
            //currentTime = t
            currentTime = (Time.time - startTime) / duration;
            
            if (isMovingFromStartToEnd)
            {
                enemy.position = Vector3.Lerp(startWaypoint.position, endWaypoint.position, currentTime);
            }
            else
            {
                enemy.position = Vector3.Lerp(endWaypoint.position, startWaypoint.position, currentTime);
            }

            if (currentTime >= 1f)
            {
                enemyRenderer.flipX = !enemyRenderer.flipX; //odwrócenie wartości boola
                isMovingFromStartToEnd = !isMovingFromStartToEnd;
                Init();
            }
        }
    }
}