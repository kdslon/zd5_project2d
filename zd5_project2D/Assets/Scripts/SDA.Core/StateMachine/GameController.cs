using SDA.Enemy;
using SDA.Player;
using SDA.UI;
using UnityEngine;

namespace SDA.Core
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private MenuView menuView;
        public MenuView MenuView => menuView; // {get{return menuView;}}
        
        [SerializeField]
        private GameView gameView;
        public GameView GameView => gameView;
        
        [SerializeField]
        private LoseView loseView;
        public LoseView LoseView => loseView;

        [SerializeField]
        private PlayerMovement playerMovement;
        public PlayerMovement PlayerMovement => playerMovement;

        [SerializeField]
        private PointsSystem pointsSystem;
        public PointsSystem PointsSystem => pointsSystem;

        [SerializeField]
        private EnemyController enemyController;
        public EnemyController EnemyController => enemyController;
        
        private BaseState currentState;

        private void Start()
        {
            //inicjalizacja zmiennych
            //ustawienie startowego stanu
            
            ChangeState(new MenuState());
        }

        private void Update()
        {
            currentState?.UpdateState();
        }

        private void FixedUpdate()
        {
            currentState?.FixedUpdateState();
        }

        private void OnDestroy()
        {
            //co ma się stać przed wyłaczeniem gry -> np. save
        }
        
        public void ChangeState(BaseState newState)
        {
            currentState?.DestroyState(); //if(currentState != null) -> ?
            currentState = newState;
            currentState?.InitState(this);
        }
    }
}