using UnityEngine.SceneManagement;

namespace SDA.Core
{
    public class LoseState : BaseState
    {
        public override void InitState(GameController gC)
        {
            base.InitState(gC);
            gC.LoseView.ShowView();
            
            gC.LoseView.RestartButton.onClick.AddListener(()=>SceneManager.LoadScene(SceneManager.GetActiveScene().name));
        }

        public override void UpdateState()
        {
            
        }

        public override void FixedUpdateState()
        {
            
        }

        public override void DestroyState()
        {
            gC.LoseView.RestartButton.onClick.RemoveAllListeners();
            gC.LoseView.HideView();
        }
    }
}