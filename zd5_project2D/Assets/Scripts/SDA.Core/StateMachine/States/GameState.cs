namespace SDA.Core
{
    public class GameState : BaseState
    {
        public override void InitState(GameController gC)
        {
            base.InitState(gC);
            gC.GameView.ShowView();
            gC.PlayerMovement.Init();
            
            gC.PointsSystem.SetPoints(0);
            gC.GameView.UpdatePoints(gC.PointsSystem.Points);
            
            gC.PlayerMovement.AddListener(AddPoint);
            gC.PlayerMovement.AddOnEnemyHitListener(GameOver);
            gC.EnemyController.InitEnemies();
        }

        public override void UpdateState()
        {
            gC.EnemyController.UpdateEnemiesPos();
        }

        public override void FixedUpdateState()
        {
            gC.PlayerMovement.UpdatePosition();
        }

        public override void DestroyState()
        {
            gC.GameView.HideView();
            gC.PlayerMovement.RemoveAllListeners();
            gC.PlayerMovement.Dispose();
        }

        private void AddPoint()
        {
            gC.PointsSystem.AddPoint();
            gC.GameView.UpdatePoints(gC.PointsSystem.Points);
        }

        private void GameOver()
        {
            gC.ChangeState(new LoseState());
        }
    }
}