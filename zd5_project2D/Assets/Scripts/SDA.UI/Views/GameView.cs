using TMPro;
using UnityEngine;

namespace SDA.UI
{
    public class GameView : BaseView
    {
        [SerializeField]
        private TextMeshProUGUI pointsText;

        public void UpdatePoints(int currentPoints)
        {
            pointsText.text = currentPoints.ToString();
        }
    }
}
