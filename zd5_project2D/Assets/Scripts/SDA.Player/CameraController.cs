using UnityEngine;

namespace SDA.Player
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody2D rb2D;

        [SerializeField]
        private Vector2 cameraBoundaries; //x->min ; y->max

        public void UpdateMovement(Vector2 speed)
        {
            rb2D.velocity = new Vector2(speed.x, 0f);
        }

        public void UpdateCameraHeight(float yPos)
        {
            yPos = Mathf.Clamp(yPos, cameraBoundaries.x, cameraBoundaries.y);
            transform.position = new Vector2(transform.position.x, yPos);
        }

        public void StopMovement()
        {
            rb2D.velocity = Vector2.zero;
        }
    }
}