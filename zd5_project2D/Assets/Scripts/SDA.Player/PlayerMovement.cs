using System;
using UnityEngine;

namespace SDA.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private const string HORIZONTAL_AXIS = "Horizontal";
        private const string SPRINT_AXIS = "Sprint";
        private const string JUMP_AXIS = "Jump";

        [SerializeField]
        private BoxCollider2D boxCollider2D;
        
        [SerializeField]
        private Rigidbody2D rigidbody2D;

        [SerializeField]
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private CameraController cameraController;

        [SerializeField]
        private float playerSpeed;

        [SerializeField]
        private float sprintMultiplier;

        [SerializeField]
        private float maxWalkSpeed;

        [SerializeField]
        private float maxRunSpeed;

        private Action onCoinCollected;
        private Action onEnemyHit;

        private bool isJumpButtonClicked;
        private float facingDirection = 1;
        
        public void Init()
        {
            //ustaw startową pozycję
            gameObject.SetActive(true);
        }

        public void UpdatePosition()
        {
            float playerInput = Input.GetAxisRaw(HORIZONTAL_AXIS);
            float sprintInput = Input.GetAxisRaw(SPRINT_AXIS);
            float jumpInput = Input.GetAxisRaw(JUMP_AXIS);
            
            var floorHitInfo = Physics2D.BoxCast(this.transform.position, 
                boxCollider2D.size, 0f, 
                Vector2.down, .1f);

            if (playerInput == 0 && floorHitInfo.collider != null)
                rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);

            if (jumpInput == 0)
                isJumpButtonClicked = false;
            
            float finalMultiplier = playerInput * playerSpeed;
            if (sprintInput > 0 && floorHitInfo.collider != null)
            {
                finalMultiplier *= sprintMultiplier; // finalMultiplier = finalMultiplier * sprintMultiplier
            }
            
            if (jumpInput > 0 && !isJumpButtonClicked)
            {
                isJumpButtonClicked = true;
                
                if (floorHitInfo.collider != null && floorHitInfo.collider.CompareTag("JumpFloor"))
                {
                    rigidbody2D.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                }
            }

            //right -> (1;0); left -> (-1;0)
            rigidbody2D.AddForce(Vector2.right * finalMultiplier, ForceMode2D.Force);
            
            var x = rigidbody2D.velocity.x;
            var direction = Mathf.Sign(x); // -1
            x = Mathf.Abs(x); // |-5| -> 5

            if (x > 0)
            {
                facingDirection = direction;
                spriteRenderer.flipX = facingDirection == -1;
            }

            var movementHitInfo = Physics2D.Raycast(transform.position, Vector2.right * facingDirection, 5f, 1 << 3);
            if (movementHitInfo.collider != null)
            {
                cameraController.UpdateMovement(rigidbody2D.velocity);
            }
            else
            {
                cameraController.StopMovement();
            }

            if (sprintInput == 0)
            {
                x = Mathf.Clamp(x, 0, maxWalkSpeed);
                //x = 5
                //Clamp(x, 0,3) -> x = 3
                //x  = 2
                //Clam(x, 0,2) -> x = 2
            }
            else
            {
                x = Mathf.Clamp(x, 0, maxRunSpeed);
            }

            Vector2 finalVelocity = new Vector2(x * direction, rigidbody2D.velocity.y);
            rigidbody2D.velocity = finalVelocity;
            cameraController.UpdateCameraHeight(transform.position.y);
        }

        public void AddListener(Action listener)
        {
            //onCoinCollected = onCoinCollected + listener
            
            onCoinCollected += listener;
        }

        public void AddOnEnemyHitListener(Action listener)
        {
            onEnemyHit += listener;
        }

        public void RemoveAllListeners()
        {
            onCoinCollected = null;
            onEnemyHit = null;
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Coin"))
            {
                onCoinCollected?.Invoke(); //<=> if(coinCollected != null) Invoke();
                Destroy(col.gameObject);
            }

            if (col.CompareTag("Enemy"))
            {
                onEnemyHit?.Invoke();
            }
        }

        public void Dispose()
        {
            cameraController.StopMovement();
            gameObject.SetActive(false);
        }
    }
}